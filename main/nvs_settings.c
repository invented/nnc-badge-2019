#include "settings.h"

void settings_setU32(const char *name, uint32_t value) {
  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
  nvs_set_u32(my_handle, name, value);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

uint32_t settings_getU32(const char *name) {
  uint32_t value = 0;

  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
  nvs_get_u32(my_handle, name, &value);
  nvs_close(my_handle);

  return value;
}

void settings_setI32(const char *name, int32_t value) {
  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
  nvs_set_i32(my_handle, name, value);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

int32_t settings_getI32(const char *name) {
  int32_t value = 0;

  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
  nvs_get_i32(my_handle, name, &value);
  nvs_close(my_handle);

  return value;
}

void settings_setString(const char *name, const char *value) {
  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
  nvs_set_str(my_handle, name, value);
  nvs_commit(my_handle);
  nvs_close(my_handle);
}

void settings_getString(const char *name, char *value, size_t maxLen) {
  nvs_handle my_handle;
  nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
  nvs_get_str(my_handle, name, value, &maxLen);
  nvs_close(my_handle);
}