#include "cmd_system.h"
#include "leds.h"
#include "settings.h"

#ifdef CONFIG_FREERTOS_USE_STATS_FORMATTING_FUNCTIONS
#define WITH_TASKS_INFO 1
#endif

static struct {
  // struct arg_int *task;
  struct arg_str *token;
  struct arg_end *end;

} flag_verify_args;

static struct {
  struct arg_int *perc;
  struct arg_end *end;

} brightness_args;

// static void register_free();
static void register_exit();
// static void register_heap();
static void register_version();
static void register_restart();
static void register_wifi();
#if WITH_TASKS_INFO
static void register_tasks();
#endif

static const char *TAG = "SHELL";
extern const uint8_t pubkey_start[] asm("_binary_ecpubkey_pem_start");

static int exit_shell(int argc, char **argv) {
  if (!os_isRooted()) {
    printf("Only root can do this\r\n");
    return 0;
  }

  static int enabled = 0;

  if (enabled) {
    enabled = 0;
    esp_log_level_set("*", ESP_LOG_NONE);
  } else {
    /* code */
    enabled = 1;
    esp_log_level_set("*", ESP_LOG_DEBUG);
  }

  return 0;
}

/* 'version' command */
static int get_version(int argc, char **argv) {
  esp_chip_info_t info;
  esp_chip_info(&info);

  char data[256] = {0};

  uint8_t *did = nnc_device_id();
  uint8_t *userid = nnc_user_id();

  printf(LOG_COLOR_W "DeviceID: %02x%02x%02x%02x%02x%02x\r\n" LOG_RESET_COLOR,
         did[0], did[1], did[2], did[3], did[4], did[5]);

  printf(LOG_COLOR_W "UserID: %02x%02x%02x\r\n" LOG_RESET_COLOR, userid[0],
         userid[1], userid[2]);

  printf("Firmware Version: %s\r\n", NNC_FW_VERSION);
  settings_getString(DSA_HASH1, data, 256);
  printf("Firmware Digest: %s\r\n", data);
  
  settings_getString(DSA_SIGN1, data, 256);
  printf("Firmware Signature: %s\r\n", data);
  
  settings_getString(DSA_HASH2, data, 256);
  printf("Partition Table Digest: %s\r\n", data);
  
  settings_getString(DSA_SIGN2, data, 256);
  printf("Partition Table Signature: %s\r\n", data);

  printf("Bootloader PubKey:\r\n%s\r\n", (char *)pubkey_start);

  printf("Chip info:\r\n");
  printf("\tmodel:%s\r\n", info.model == CHIP_ESP32 ? "ESP32" : "Unknown");
  printf("\tcores:%d\r\n", info.cores);
  printf("\tfeature:%s%s%s%s%d%s\r\n",
         info.features & CHIP_FEATURE_WIFI_BGN ? "/802.11bgn" : "",
         info.features & CHIP_FEATURE_BLE ? "/BLE" : "",
         info.features & CHIP_FEATURE_BT ? "/BT" : "",
         info.features & CHIP_FEATURE_EMB_FLASH ? "/Embedded-Flash:"
                                                : "/External-Flash:",
         spi_flash_get_chip_size() / (1024 * 1024), " MB");
  printf("\trevision number:%d\r\n", info.revision);
  return 0;
}

/** Arguments used by 'join' function */
static struct {
  struct arg_int *timeout;
  struct arg_str *ssid;
  struct arg_str *password;
  struct arg_end *end;
} join_args;

static int connect(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **)&join_args);
  if (nerrors != 0) {
    arg_print_errors(stderr, join_args.end, argv[0]);
    return 1;
  }

  bool connected =
      wifi_join(join_args.ssid->sval[0], join_args.password->sval[0],
                join_args.timeout->ival[0]);
  if (!connected) {
    printf("Connection timed out :( \r\n");
    return 1;
  }

  settings_setString("wifi-user-ap", join_args.ssid->sval[0]);
  settings_setString("wifi-user-pass", join_args.password->sval[0]);
  
  printf("WiFi Connected, credentials saved\r\n");

  return 0;
}

static int brightness(int argc, char **argv) {
  int nerrors = arg_parse(argc, argv, (void **)&brightness_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, brightness_args.end, argv[0]);
    return 0x101;
  }

  led_setBrightness(brightness_args.perc->ival[0]);

  return 0;
}

static int flag_verify(int argc, char **argv) {
  if (!os_isRooted()) {
    printf("Only root can do this\r\n");
    return 0;
  }

  int nerrors = arg_parse(argc, argv, (void **)&flag_verify_args);

  if (nerrors != 0) {
    arg_print_errors(stderr, flag_verify_args.end, argv[0]);
    return 0x101;
  }

  char *message =
      strdup("Invalid flag. Go find something like: flag{example}\r\n");

  uint8_t flagIndex = os_isValidFlag(flag_verify_args.token->sval[0], &message);

  if (flagIndex) {
    os_writeFlag(flagIndex, flag_verify_args.token->sval[0]);
    printf(LOG_COLOR_W "Congrats! Challenge solved!\r\n%s\r\n" LOG_RESET_COLOR,
           message);
    os_printFlags();
  } else {
    printf(message);
  }

  free(message);
  return 0;
}

static int flags_print(int argc, char **argv) {
  if (!os_isRooted()) {
    printf("Only root can do this\r\n");
    return 0;
  }
  os_printFlags();
  return 0;
}

/** 'tasks' command prints the list of tasks and related information */
#if WITH_TASKS_INFO

static int tasks_info(int argc, char **argv) {
  if (!os_isRooted()) {
    printf("Only root can do this\r\n");
    return 0;
  }

  const size_t bytes_per_task = 40; /* see vTaskList description */
  char *task_list_buffer = malloc(uxTaskGetNumberOfTasks() * bytes_per_task);
  if (task_list_buffer == NULL) {
    ESP_LOGE(TAG, "failed to allocate buffer for vTaskList output");
    return 1;
  }
  fputs("Task Name\tStatus\tPrio\tHWM\tTask#", stdout);
#ifdef CONFIG_FREERTOS_VTASKLIST_INCLUDE_COREID
  fputs("\tAffinity", stdout);
#endif
  fputs("\n", stdout);
  vTaskList(task_list_buffer);
  fputs(task_list_buffer, stdout);
  free(task_list_buffer);
  return 0;
}

#endif // WITH_TASKS_INFO

static void register_system() {
  register_version();
  register_restart();
  register_exit();
  register_wifi();
#if WITH_TASKS_INFO
  register_tasks();
#endif
}

static void register_flags_cmd() {
  const esp_console_cmd_t command = {
      .command = "flags",
      .help = "Prints your flags and sends them to Spynet",
      .hint = NULL,
      .func = &flags_print};

  ESP_ERROR_CHECK(esp_console_cmd_register(&command));
}

static void register_brightness_cmd() {
  brightness_args.perc =
      arg_int1(NULL, NULL, "<0..100>", "Brightness, int");
  brightness_args.end = arg_end(1);

  const esp_console_cmd_t command = {.command = "brightness",
                                     .help = "Set LEDs brightness in perc",
                                     .hint = NULL,
                                     .func = &brightness,
                                     .argtable = &brightness_args};

  ESP_ERROR_CHECK(esp_console_cmd_register(&command));
}

static void register_token_cmd() {
  flag_verify_args.token =
      arg_str1(NULL, NULL, "<flag{example}>", "Flag, string");
  flag_verify_args.end = arg_end(1);

  const esp_console_cmd_t command = {.command = "verify",
                                     .help = "Verify CTF flag",
                                     .hint = NULL,
                                     .func = &flag_verify,
                                     .argtable = &flag_verify_args};

  ESP_ERROR_CHECK(esp_console_cmd_register(&command));
}

static void register_wifi() {
  join_args.timeout =
      arg_int0(NULL, "timeout", "<t>", "Connection timeout, ms");
  join_args.timeout->ival[0] = 5000; // set default value
  join_args.ssid = arg_str1(NULL, NULL, "<ssid>", "SSID of AP");
  join_args.password = arg_str0(NULL, NULL, "<pass>", "PSK of AP");
  join_args.end = arg_end(2);

  const esp_console_cmd_t join_cmd = {.command = "join",
                                      .help = "Join WiFi AP as a station",
                                      .hint = NULL,
                                      .func = &connect,
                                      .argtable = &join_args};

  ESP_ERROR_CHECK(esp_console_cmd_register(&join_cmd));
}

static int restart(int argc, char **argv) { esp_restart(); }

static void register_factory_reset() {
  const esp_console_cmd_t cmd = {
      .command = "factory_reset",
      .help = "Reset CTF results and generate new deviceId",
      .hint = NULL,
      .func = &nnc_reset_ctf,
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static void register_restart() {
  const esp_console_cmd_t cmd = {
      .command = "restart",
      .help = "Software reset of the chip",
      .hint = NULL,
      .func = &restart,
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static void register_version() {
  const esp_console_cmd_t cmd = {
      .command = "version",
      .help = "Get version of chip and SDK",
      .hint = NULL,
      .func = &get_version,
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

static void register_exit() {
  const esp_console_cmd_t cmd = {
      .command = "dmsg",
      .help = "Toggle BadgeOS logs",
      .hint = NULL,
      .func = &exit_shell,
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

#if WITH_TASKS_INFO
static void register_tasks() {
  const esp_console_cmd_t cmd = {
      .command = "ps",
      .help = "Get information about running tasks",
      .hint = NULL,
      .func = &tasks_info,
  };
  ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}
#endif

void register_cmds() {
  register_system();
  register_token_cmd();
  register_flags_cmd();
  register_factory_reset();
  register_brightness_cmd();
}
