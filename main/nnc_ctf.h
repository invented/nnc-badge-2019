#pragma once

#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#define UNUSED(expr)                                                           \
  do {                                                                         \
    (void)(expr);                                                              \
  } while (0)

typedef struct {
  char flags[10][50];
} badge_flags;

bool os_isRooted();
bool os_isSolved(int task);
uint8_t os_isValidFlag(const char *flag, char **msg);
void os_setRoot(bool obtained);
void os_writeFlag(uint8_t index, const char *flag);
void os_printFlags();
uint8_t *nnc_device_id();
uint8_t *nnc_user_id();
char *nnc_fdf(const char *task_name);
void nnc_context_init();
int nnc_reset_ctf(int argc, char **argv);
void nnc_updateLeds();
