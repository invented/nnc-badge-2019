#pragma once

#include "app.h"
#include "argtable3/argtable3.h"
#include "driver/rtc_io.h"
#include "driver/uart.h"
#include "esp_console.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_sleep.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nnc_ctf.h"
#include "nvs_flash.h"
#include "rom/uart.h"
#include "sdkconfig.h"
#include "soc/rtc_cntl_reg.h"
#include "wifi.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define SETSNAMESPACE "nnc_data"

#define WFAP "wifi-ap"
#define WFPASS "wifi-pass"
#define WFUSERAP "wifi-user-ap"
#define WFUSERPASS "wifi-user-pass"

#define API_URL "data-url"
#define API_OTA_VERSION "ota-version"
#define API_OTA_FW "ota-firmware"

#define DSA_HASH1 "dsa-hash1"
#define DSA_SIGN1 "dsa-sign1"
#define DSA_HASH2 "dsa-hash2"
#define DSA_SIGN2 "dsa-sign2"

#define LED_BRIGHT "led-brig"

void settings_setU32(const char *name, uint32_t value);
uint32_t settings_getU32(const char *name);
void settings_setI32(const char *name, int32_t value);
int32_t settings_getI32(const char *name);
void settings_setString(const char *name, const char *value);
void settings_getString(const char *name, char *value, size_t maxLen);
