#pragma once

#include "app.h"
#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <math.h>

#define GREEN 0xFF0000
#define RED 0x00FF00
#define BLUE 0x0000FF

void led_setBrightness(int perc);
void led_setColor(int ledIndex, uint32_t color);
void led_updateAll();