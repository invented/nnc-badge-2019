#pragma once

#include "app.h"
#include "lwip/apps/sntp.h"
#include "lwip/err.h"
#include "nnc_ctf.h"
#include "settings.h"

int wifi_join(char *ssid, char *password, int timeout_ms);
void wifi_sendData(const char *flag);