#pragma once

#define NNC_FW_VERSION "v1.95"

// #define BADGE_RED
// #define MEGABADGE
#define NUM_LEDS 10

#ifdef BADGE_RED
// Red one
#define PIN_CAN_RX 21
#define PIN_CAN_TX 22
#define PIN_IIC_SCL 13
#define PIN_IIC_SDA 14
#define PIN_ADC1 34
#define PIN_ADC2 33
#define PIN_BUZZER 15
#define PIN_LED_BLUE 5
#define PIN_WS2812 17
// PIN 33 == T8
#define PIN_TOUCH 8
// 17
#else

#define PIN_CAN_RX 21
#define PIN_CAN_TX 22
#define PIN_IIC_SCL 16
#define PIN_IIC_SDA 17
#define PIN_ADC1 34
// #define PIN_ADC2 33
#define PIN_BUZZER 13
// PIN 12 == T5
#define PIN_TOUCH 5
#define PIN_LED_BLUE 5
#define PIN_WS2812 4

#endif