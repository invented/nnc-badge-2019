#include "nnc_ctf.h"
#include "leds.h"
#include "wifi.h"

static badge_flags context;

uint8_t *nnc_user_id() {
  static uint8_t id[12] = {0};
  return id;
}

uint8_t *nnc_device_id() {
  static uint8_t id[12] = {0};
  return id;
}

char *nnc_fdf(const char *task_name) {
  char *flag = strdup("flag{¯\\_(ツ)_/¯}");
  // ¯\_(ツ)_/¯
  return flag;
}

char *nnc_hidden_flag5() {
  char *flag = strdup("flag{¯\\_(ツ)_/¯}");
  // ¯\_(ツ)_/¯
  return flag;
}

bool os_isSolved(int task) {
  return os_isValidFlag(context.flags[task - 1], NULL);
}

bool os_isRooted() { return false; }

uint8_t os_isValidFlag(const char *flag, char **msg) {
  // ¯\_(ツ)_/¯
  return 0;
}

void os_setRoot(bool obtained) {
  // ¯\_(ツ)_/¯
}

void nnc_updateLeds() {
  led_setColor(0, os_isValidFlag(context.flags[0], NULL) ? RED : 0); // RE

  led_setColor(1, os_isValidFlag(context.flags[1], NULL) ? GREEN : 0); // web
  led_setColor(9, os_isValidFlag(context.flags[1], NULL) ? GREEN : 0);

  led_setColor(2, os_isValidFlag(context.flags[2], NULL) ? GREEN : 0); // web
  led_setColor(8, os_isValidFlag(context.flags[2], NULL) ? GREEN : 0);

  led_setColor(3, os_isValidFlag(context.flags[3], NULL) ? BLUE : 0);
  led_setColor(7, os_isValidFlag(context.flags[3], NULL) ? BLUE : 0);

  led_setColor(4, os_isValidFlag(context.flags[4], NULL) ? RED : 0);
  led_setColor(6, os_isValidFlag(context.flags[4], NULL) ? RED : 0);

  led_setColor(5, os_isValidFlag(context.flags[5], NULL) ? BLUE : 0);

  led_updateAll();
}

void os_printFlags() {
  // ¯\_(ツ)_/¯
}

void os_writeFlag(uint8_t index, const char *flag) {
  // ¯\_(ツ)_/¯
}

int nnc_reset_ctf(int argc, char **argv) {

  for (size_t i = 0; i < 10; i++) {
    printf(LOG_COLOR(LOG_COLOR_RED) "\rFactory reset in %d seconds. Plug it "
                                    "out if it was a mistake :)  ",
           10 - i);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }

  for (size_t i = 0; i < 10; i++) {
    context.flags[i][0] = 0;
  }

  // ¯\_(ツ)_/¯

  esp_restart();

  return 0;
}

void nnc_context_init() {
  // ¯\_(ツ)_/¯

  for (size_t i = 0; i < 10; i++) {
    if (strlen(context.flags[i]) < 5) {
      strcpy(context.flags[i], "<empty>");
    }
  }
}